// use solang to deploy
const { Connection, LAMPORTS_PER_SOL, Keypair } = require('@solana/web3.js');
const { Contract, publicKeyToHex } = require('@solana/solidity');
const { readFileSync } = require('fs');

const SWAPTOKEN_ABI = JSON.parse(readFileSync('./build/SwapToken.abi', 'utf8'));
const BUNDLE_SO = readFileSync('./build/bundle.so')

(async function () {
  console.log('Connecting to your local Solana node ...')
  const connection = new Connection(
       "https://api.devnet.solana.com",
      'confirmed'
  )
  const payer = Keypair.generate()
    while (true) {
        console.log('Airdropping (from faucet) SOL to a new wallet ...')
        await connection.requestAirdrop(payer.publicKey, 1 * LAMPORTS_PER_SOL)
        await new Promise((resolve) => setTimeout(resolve, 1000))
        if (await connection.getBalance(payer.publicKey)) break
    }

    const program = Keypair.generate()
    const storage = Keypair.generate()
    console.log(publicKeyToHex(storage.publicKey))
    const contract = new Contract(connection, program.publicKey, storage.publicKey, SWAPTOKEN_ABI, payer)

    console.log('Program deployment finished, deploying ERC20 ...')
    await contract.deploy('My Swap SC', ['mECjaTpkSQANuq3jKgogR98CzWn4iEsFotxMJ2Mtm4J'], program, storage, 4096 * 8)

    console.log('Contract deployment finished, invoking contract functions ...')
    await contract.load(program, BUNDLE_SO)

  process.exit(0);
})();