# Remitano Blockchain Dev Testing

**Requirement:**

- Create a new token on Solana testnet, for example, MOVE token.
- Create a smart contract to swap SOL to MOVE token, for each SOL swapped we will receive 10 MOVE.
- Create a script to execute to send swap transaction to Solana testnet

**How to test:**
1. Setup an environment 

- [Solang](https://solang.readthedocs.io/en/latest/installing.html) to build our smart contracts

2. Build the smart contract 

```shell
./solang-linux-x86-64 compile --target solana ~/remitanotest/contracts/SwapToken.sol -o ~/remitanotest/build -v
```

3. Deploy the smart contract

4. Run unitest