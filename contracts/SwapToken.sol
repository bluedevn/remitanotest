// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

// Uncomment this line to use console.log
// import "hardhat/console.sol";
interface IERC20 {

    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

contract SwapToken {
    uint RATE_AMOUNT  = 10; // 1 SOL = 10 MOVE

    IERC20 public moveTokenAddress;

    event Swap(address indexed from, uint amount);

    constructor(address _moveTokenAddress) {
        moveTokenAddress = (IERC20)(_moveTokenAddress);
    }

    /**
     * @dev A simple function to swap from SOL to MOVE token 
     */
    function swap()payable  external  {
        uint _solAmount = msg.value;

        require(_solAmount > 0, "Send ETH to buy some tokens");
        // Calculate the amount needed to swap 
        uint _swapAmount = _solAmount * RATE_AMOUNT;

        uint _availableBalance = moveTokenAddress.balanceOf(address(this));
        require( _availableBalance >= _swapAmount, "We out of MOVE token in pool");

        // Swap action
        (bool sent)  = moveTokenAddress.transfer(msg.sender, _swapAmount);
        require(sent, "Failed to transfer token to user");
        emit Swap(msg.sender, _solAmount);
    }
}
